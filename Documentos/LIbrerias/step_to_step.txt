1. Registarse en:

 https://pypi.org/account/register/

2. crear una carpeta 

create_lebrary

3. crear entorno virtual

virtualenv venv

3. Activar el entorno virtual

source venv/bin/activate

4. crear la estructura de la libreria

operacion
   operacion
        |->__init__.py
        |->modulo.py
   |->setup.py
   |->README.md

5. instalar las librerias

pip install twine wheel setuptools

6. generar el .whl 

python setup.py sdist bdist_wheel

7. cheaquear

twine check dist/*

8. generar el token en el API y ponerla en .pypirc que esta en el home de ubuntu

[pypi]
  username = __token__
  password = pypi-aadascasczxvcasvdasfdas


9. subir la actualizacion

twine upload dist/*
