Instalar la Librería
pip install Bono2FdoJar 

La librería contiene dos clases
    Clase para calcular una integral usando Montecarlo MontecarloIntegral()
    Clase para suavizar un conjunto de datos Kernel()

# import numpy as np
# import pandas as pd

import Bono2FdoJar as BFJ

Para usar la librería de Montecarlo
- Inicializar la clase con
    area = BFJ.MontecarloIntegral(a,b,fun_int)
    a y b -> intervalo donde se calucla la integral
    fun_int -> Función a integrar 

- Llamar el método con el valor de la integral
    sol = area.area()


Para usar el kernel gaussiano
- Inicializar la clase con
    suav = BFJ.Kernel()

-Calcular la funcion intercalada
    suav.suavizado(data_x,data_y,kernel,r)

    data_x y data_y -> coordenadas en x y y de los datos
    kernel -> Función para interpolar 
    r -> valor de r

- Guardar la grafica:
    suav.grafica("nombre_grafica")
