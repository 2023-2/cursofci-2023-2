libreria:   'ebofcis2'

Profe, cargué una librería con dos clases:

1. Integral_montecarlo, que nos permite resolver integrales por método montecarlo, comparar con solución
analitica.

    -> integral.area(integral.n), integral.analitica()
    Donde 'integral' es el objeto instanciado de la clase

2. ajuste_curva, que nos permite dado un ocnjunt ode datos csv asociado al número de casos nuevos de covid por fecha
y suavizar la curva, a través de un kernel gaussiano.

    ->data.plot(0.1) 
    Con el método 'plot' podemos obtener la visualización de los datos y la curva suavizada 0.1 corresponde al valor de r


Adjunto un colab donde hay un ejemplo de ambas clases, por si hay dudas
    https://colab.research.google.com/drive/1HjDyY3cTjURhehr5YqyWlpbGvcGmU6n8?usp=sharing

Link libreria en PyPI:
    https://pypi.org/project/ebofcis2/0.1.8/
