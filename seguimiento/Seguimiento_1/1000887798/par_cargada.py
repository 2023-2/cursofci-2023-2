import numpy as np
import matplotlib.pyplot as plt

class particula:
    
    """ ..."""

    #Método constructor 
    def __init__(self,Ek,theta,m,B,q=1.602176634e-19):

        """Método constructor de la clase particula  \
            los parámetros para inicializar la clase \
            Ek  ---> Energía cinetica en [eV]          \
            theta --> ángulo que forma el vector [grados]    \
                      velocidad con B, campo magnetico\
            m --> masa de la particula [kg]\
            B --> Magnitud del campo magnetico[T] en z"""

        self.Ek = Ek * 6.24e18    #Pasar energía cinetica a Joules
        self.theta = np.deg2rad(theta)
        self.m = m
        self.B = B
        self.q = q
        self.vc = np.zeros(3)   #Vector velocidad
        self.r = np.zeros(3)    #Vector posición
        self.strings = [str(Ek),str(theta),str(m),str(B)]


    
    #Métodos de la clase

    #Método para obtener la magnitud velocidad
    def vel(self):

        """Método que calcula la velocidad de la particula """
        try:
            
            return np.sqrt(2*self.Ek*self.q/self.m)
        except:
            print("\033[1;31m"+"Error al calcular la velocidad" +'\033[0;m')

    #Método para la frecuencia ciclotronica
    def W(self):

        """Método que calcula la frecuencia ciclotronica """

        try:
            w = self.q*self.B/self.m
            if w == 0:
                print("\033[1;31m"+"No hay campo magnético" +'\033[0;m')
            return w
        except:
            print("\033[1;31m"+"Error al calcular la frecuencia ciclotrónica" +'\033[0;m')

    #Método para obtener la velocidad inicial en cada componente
    def velc(self):

        "Metodo que calcula la velocidad en cada componente"

        try: 
            vx = self.vel()*np.cos(self.theta)  #Componente en x
            vz = self.vel()*np.sin(self.theta)  #Componente en y
            vy = 0                              #Componente en z                        
            self.vc = np.array([vx,vy,vz])
            return self.vc
        except:
            print("\033[1;31m"+"Error al calcular la velocidad en cada componente" +'\033[0;m') 

    #Método para obtener la posición en cada componente
    def posc(self,t):

        """Método que calcula la posición en cada componente
            recibe como parámetro el tiempo (segundos) """
        
        vc = self.velc()
        self.W()
        try:
            ry = (vc[0]/self.W())*(np.cos(self.W()*t)-1) #Componente en y
            rx = (vc[0]/self.W())*np.sin(self.W()*t)     #Componente en x
            rz = vc[2]*t                                 #Componente en z

            self.r = np.array([rx,ry,rz])
            return self.r
        except:
            print("\033[1;31m"+"Error al calcular la posición en cada componente" +'\033[0;m')


    #Método para obtener el gráfico de la trayectoria

    def grafica(self,t,n):

        """ Método pára obtener la gráfica de la trayectoria
            recibe como parámetro el tiempo y n el numero de interaciones
            (como la frecuencia ciclotronica es muy alta se 
            recomienda tomar un timpo pequeño para poder obtener
            gráfico más diciente) """
        
        try:
            
            t = np.linspace(0,t,n)

            rgraf = self.posc(t)    
            plt.style.use("seaborn")
            ax = plt.figure().add_subplot(projection='3d')
            
            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_zlabel("z")
            
            plt.title(fr"Trayectoria de una particula con  $B_z$={self.strings[3]}[T], $\theta$ = {self.strings[1]}° y  $E_k$={self.strings[0]}[eV]")

            ax.plot(rgraf[0],rgraf[1],rgraf[2],label="Trayectoria")
            ax.legend()


            plt.savefig("trayectoria.png")
            
            return print("\033[1;32m"+"Imagen guardada!"+'\033[0;m')
        except:
            print("\033[1;31m"+"Error al graficar la trayectoria"+'\033[0;m')



