from PartMagField import ParticleMagField
import numpy as np

"""
NOTA

4.2

- Muy buen trabajo
- No funciona con B=0

"""

if __name__=="__main__":
    # Definir entradas
    m = 9.11e-31 #masa en kg 
    q = 1.602e-19 #carga en Coulombs
    E = 0.3  #energia en electronvoltios
    pol =np.radians(30) #angulo polar en radianes
    B = 0#00e-6   #intensidad de B en tesla

    Part=ParticleMagField(m,q,E,pol,B)
    Part.PosGraph()