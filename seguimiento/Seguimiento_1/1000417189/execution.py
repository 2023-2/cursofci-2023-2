from partCarg import partCarg

"""
NOTA

5.0

- Muy buen trabajo

"""

if __name__=="__main__":
    print("Procesando...")

    Ek = 18.6   #eV
    grad = 30  #° 
    B = 300e-6 #µT

    prob=partCarg(Ek,grad,B)

    prob.plot()