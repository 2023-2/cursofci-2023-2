import numpy as np
import matplotlib.pyplot as plt

class partCarg:
    """
    Esta clase calcula la trayectoria de una partícula cargada en presencia de un campo
    magnético en 3 dimensiones.
    Recibe valores numéricos de la energía de la partícula en electronvoltios, el ángulo
    de la velocidad con respecto al campo en grados, la intensidad del campo magnético.
    La carga en Coulumbs y la masa en kilogramos, cuyos valores deafault son para el electrón.
    Y el número de iteraciones y paso para la trayectoria.
    """
    def __init__(self,Energia, angulo, campo,\
                  carga=-1.602176634e-19,masa=9.1093837015e-31, iteraciones=10000,paso=0.01):
        self.E=Energia
        self.th=angulo
        self.B=campo
        self.q=carga
        self.m=masa
        
        self.i=iteraciones
        self.p=paso

        self.trans()    #Conversor de unidades

    def t(self):
        """
        Crea un linspace del parámetro temporal 
        desde cero hasta iteraciones * paso, con el número de iteraciones
        especificado.
        """
        # t=np.linspace(0,0.000001,1000)#para que salga lindo
        t=np.linspace(0,self.i*self.p,self.i)
        return t

    def EvToJoules(self):
        """
        Convierte la energía de eV a Joules
        """
        self.E=self.E*1.60218e-19
    
    def GradosToRad(self):
        """
        Convierte el ángulo de grados a radianes
        """
        self.th=self.th*np.pi/180

    def trans(self):
        """
        Efectúa y comprueba los método de cambio de unidades
        """
        try:
            self.EvToJoules()
            self.GradosToRad()
        except:
            print("Ingrese datos válidos\n")

    def v(self):
        """Calcula la velocidad basado en la energía y la masa
        """
        try:
            if self.E > 0 and self.m > 0:
                v = (2*self.E/self.m)**0.5 

                if v/3e8 > 0.05:    #si la velocidad es demasiado grande las hipótesis del
                                    #problema se dejan de cumplir
                    print("El problema está alcanzando límites relativistas: {}% de c\n"\
                    .format(round(self.v()/3e8*100,3)))
                return v
            else:
                print("Ingrese valores apropiados para masa y energía")
        except:
            print("No se pudo calcular la velocidad")
    
    def wc(self):
        """
        Calcula la frecuencia ciclotrónica
        """
        try:
            wc = self.q*self.B/self.m
            return wc
        except:
            print("Ingrese datos válidos\n")
    
    def v0x(self):
        """
        Calcula la velocidad inicial en X
        """
        return self.v()*np.sin(self.th)
    
    def v0z(self):
        """
        Calcula la velocidad inicial en Z
        """
        return self.v()*np.cos(self.th)
    
    def y(self):
        """
        Calcula la componente Y de la trayectoria,
        siendo esta la dirección perpendicular a la 
        proyección de la velocidad sobre el plano del que 
        el campo es vector normal
        """
        try:
            if self.wc() != 0:
                return self.v0x()/self.wc()*( np.cos( self.wc()*self.t() ) - 1)
            
            else:   #si la frecuencia es cero no se descarta sino que se toma como MRU
                return 0*self.t()
        except:
            print("No se pudo calcular Y")
    
    def x(self):
        """
        Calcula la componente X de la tryectoria,
        siendo esta la dirección de la poryección de
        la velocidad sobre el plano del que el campo
        es vector normal
        """
        try:
            if self.wc() != 0:
                return self.v0x()/self.wc()* np.sin(self.wc()*self.t())
            else:   #si la frecuencia es cero no se descarta sino que se toma como MRU
                return self.v0x()*self.t()
        except:
            print("No se pudo calcular X")

    def z(self):
        """
        Calcula la componente Z de la trayectoria,
        siendo esta la dirección del campo
        """
        try:
            return self.v0z()*self.t()
        except:     #si la frecuencia es cero no se descarta sino que se toma como MRU
            print("No se pudo calcular Z")

    def traj(self):
        """
        Retorna un array con las 3 componentes de la trayectoria
        """
        return np.array([self.x(),self.y(),self.z()])
    
    def plot(self,name="plot"):
        """
        Hace una gráfica 3D de la trayectoria
        con Z en la dirección del campo
        Y la guarda con el nombre name.png
        """
        print("Graficando...")
        try:
            fig = plt.figure(figsize=(10,10))
            ax = fig.add_subplot(111, projection="3d")
            ax.plot(self.x(), self.y(), self.z())
            
            ax.set_xlabel("X (m)")
            ax.set_ylabel("Y (m)")
            ax.set_zlabel("Z (m)\n (dirección del campo)")
            ax.set_title("Trayectoria con {} iteraciones con paso {}".format(self.i,self.p))

            plt.savefig(name)
            plt.show()
            print("Gráfica guardada")
        except:
            print("No se pudo graficar")
        