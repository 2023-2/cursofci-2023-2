import numpy as np
import matplotlib.pyplot as plt

class particle_in_magnetic_field:
    
    """
    Grafica la trayectoria de una partícula cargada (configurado en este caso para un electrón) 
    con cierta velocidad, cuando esta atraviesa un campo magnético uniforme en la dirección z
    
    Params:
        E_k: Energía cinética de la partícula (Se debe dar en eV) 
        theta: Ángulo entre el campo y la velocidad de la partícula (Grados) 
        m: Masa de la partícula (Electrón)(En kg)(Debe ser mayor que 0)
        B: Intensidad de campo magnético  (En microTesla) 
        phi: Ángulo acimutal, se supone cero para que el vector velocidad quede sobre el plano XZ
        q: Carga de la partícula (Electrón)(En coulombs)(Debe ser distinta de 0)
        
    Returns:
        Reconstruye la trayectoria (x,y,z) seguida por la partícula en 
        un tiempo igual a 4 veces su periodo
    
    """
    
    def __init__(self,E_k,theta,B,phi = 0, m=9.1e-31, q=1.6e-19):   
        try:
            self.E_k = E_k/6.242e+18     #Conversión de eV a J
            self.theta = np.radians(theta)   #Conversión de grados a radianes
            self.m = m
            self.B = B*1e-6   #Conversión de MicroTesla a Tesla
            self.q = q
            self.phi = np.radians(phi)     
        except ValueError as e:
            print(f"Error en la inicialización: {str(e)}")
        
    def r(self):   #Radio de la trayectoria 
        try: 
            return (self.m*self.v()) / (self.q*self.B)
        except ZeroDivisionError as e:
            print("Error: División por cero al calcular el radio.")
            return None
    
    def w_c(self):   #Frecuencia ciclotrónica
        try:
            return  self.q*self.B/self.m
        except ZeroDivisionError as e:
            print("Error: División por cero al calcular la frecuencia ciclotrónica.")
            return None
    
    def T(self):    #Periodo
        try:
            return 2*np.pi/self.w_c()
        except ZeroDivisionError as e:
            print("Error: División por cero al calcular el período.")
            return None
    
    def t(self):  #Array de tiempo
        try:
            return np.linspace(0, 4*self.T(), 2000)
        except ZeroDivisionError as e:
            print("Error: División por cero al calcular el array de tiempo.")
            return None
    
    def v(self):   #Velocidad de la partícula
        try:
            v_0 = np.sqrt(2*self.E_k/self.m)
            v_0x = v_0*np.sin(self.theta)*np.cos(self.phi)
            v_0y = v_0*np.sin(self.theta)*np.sin(self.phi)
            v_0z = v_0*np.cos(self.theta)
            return v_0x, v_0y, v_0z
        except ValueError as e:
            print(f"Error al calcular la velocidad: {str(e)}")
            return None
    
    
    def trajectory(self):
        try:
            v_0x, v_0y, v_0z = self.v()
            x = (v_0x/self.w_c())*(np.cos(self.w_c()*self.t())-1)
            y = (v_0x/self.w_c())*(np.sin(self.w_c()*self.t()))
            z = v_0z*self.t()
            return x, y, z
        except Exception as e:
            print(f"Error al calcular la trayectoria: {str(e)}")
            return None
    
    def plot_trayectory(self):
        try:
            x, y, z = self.trajectory()
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot(x, y, z, label='Trayectoria de la partícula')
            final_point = (0, 0, 0)
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
            plt.savefig('Particle_trajectory')
            plt.legend()
            plt.show()
        except Exception as e:
            print(f"Error al graficar la trayectoria: {str(e)}") 
        
        
        
