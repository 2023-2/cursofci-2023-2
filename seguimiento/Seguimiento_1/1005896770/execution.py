
from seguimiento1 import trayectoria

"""
NOTA

5.0

- Muy buen trabajo
- Se recomienda definir métodos para realizar cálculos,
evitar hacerlo en el método constructor.

"""

if __name__ == '__main__':

    energia = 0.3 #eV
    angulo = 40 #grados
    campoB = 700e-0 #Tesla
    masa = 9.1093837e-31 #Kg

    particula = trayectoria(energia, angulo, campoB, masa)

    particula.figPlot()
