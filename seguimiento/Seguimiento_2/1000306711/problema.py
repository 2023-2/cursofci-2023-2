import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib import cm

class HeatDiffusionSimulation:
    def __init__(self, initial_temperatures, num_steps, grid_size):
        self.grid_size = grid_size
        self.num_steps = num_steps
        self.temperatures = initial_temperatures.reshape(grid_size, grid_size)

    def simulate(self):
        if self.num_steps<=0:
            raise ValueError("El número de pasos debe ser mayor a cero")
        if self.grid_size<=0:
            raise ValueError("El tamaño de la grilla debe ser mayor a cero")
        
        all_temperatures = [self.temperatures.copy()]

        for _ in range(self.num_steps):
            new_temperatures = self.temperatures.copy()
            for i in range(1, self.grid_size - 1):
                for j in range(1, self.grid_size - 1):
                    delta = np.random.uniform(0, 1)
                    new_temperatures[i, j] = 0.2 * (self.temperatures[i-1, j] + self.temperatures[i+1, j] +
                                                   self.temperatures[i, j-1] + self.temperatures[i, j+1] - 4 * self.temperatures[i, j]) * delta + self.temperatures[i, j]

            self.temperatures = new_temperatures
            all_temperatures.append(self.temperatures.copy())
        return all_temperatures    
    
    def plot(self):
        all_temperatures = self.simulate()
        X, Y = np.meshgrid(np.arange(0, self.grid_size), np.arange(0, self.grid_size))
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, projection="3d")
        ax.plot_surface(X, Y, all_temperatures[-1], rstride=1, cstride=1, cmap=cm.inferno, edgecolor="none")
        ax.set_title("Difusión de calor en una placa metálica")
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        ax.set_zlabel("Temperatura")
        ax.set_zlim(0, 100)
        ax.view_init(elev=15, azim=30)
        plt.show()

class AnimatedHeatDiffusionSimulation(HeatDiffusionSimulation):
    def animate_simulation(self, fps=10):
        if self.num_steps<=0:
            raise ValueError("El número de pasos debe ser mayor a cero")
        if self.grid_size<=0:
            raise ValueError("El tamaño de la grilla debe ser mayor a cero")
        if self.temperatures[0][0]<0:
            raise ValueError("La temperatura debe ser mayor a cero")
        
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, projection="3d")
        ax.set_title("Difusión de calor en una placa metálica")

        def animate(frame):
            ax.clear()
            ax.set_title("Difusión de calor en una placa metálica")
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            ax.set_zlabel("Temperatura (C°)")
            ax.view_init(elev=15, azim=30)
            ax.set_zlim(0, 100)
            ax.plot_surface(X, Y, all_temperatures[frame], cmap=cm.hot, edgecolor="none")


        X, Y = np.meshgrid(np.arange(0, self.grid_size), np.arange(0, self.grid_size))
        all_temperatures = [self.temperatures.copy()]
        for _ in range(self.num_steps):
            new_temperatures = self.temperatures.copy()
            for i in range(1, self.grid_size - 1):
                for j in range(1, self.grid_size - 1):
                    delta = np.random.uniform(0, 1)
                    new_temperatures[i, j] = 0.2 * (self.temperatures[i-1, j] + self.temperatures[i+1, j] +
                                                   self.temperatures[i, j-1] + self.temperatures[i, j+1] - 4 * self.temperatures[i, j]) * delta + self.temperatures[i, j]

            self.temperatures = new_temperatures
            all_temperatures.append(self.temperatures.copy())

        ani = FuncAnimation(fig, animate, frames=self.num_steps, interval=1000/fps)
        ani.save("difusion_calor.gif", writer="imagemagick", fps=fps)