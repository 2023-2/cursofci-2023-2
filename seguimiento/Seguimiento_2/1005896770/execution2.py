from punto2 import Ising

if __name__ == '__main__':
    J = 2
    n = 1000
    beta = 100
    N = 100
    primero = Ising(J, n, beta, N)

    primero.figMagnetizacion()
    primero.plots()