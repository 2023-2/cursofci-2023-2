import numpy as np
import matplotlib.pyplot as plt
import sympy as sp
# import matplotlib.animation as animation
from PIL import Image
import os
import sys
sys.path.append('..')   #Para poder importar el modulo montecarlo con la clase Integral_montecarlo

from Punto1.montecarlo import Integral_montecarlo

# montecarlo.IntegralMonteCarlo


class QM(Integral_montecarlo):
    """Clase para encontrar la probabilidad de encontrar una particula en un dx 
    dado una funcion de onda psi(x)
    Viene heredad de la clase IntegralMonteCarlo del modulo montecarlo"""

    def __init__(self, psi, a,b,n):

        """Inicializa la clase con la funcion de onda psi(x) debe ser una funcion
        de sympy, a y b son los limites de la integral y N es el numero de iterecaiones"""
        super().__init__(psi, a, b, n)
        self.psi = psi
        self.a = a
        self.b = b
        self.N = n
        

    def plot_psi(self):
        """Grafica la funcion de onda psi(x)**2"""
        try:
            x = np.linspace(self.a, self.b, 1000)
            plt.plot(x, self.psi(x)**2)
            plt.title(f"Funcion de onda psi(x)**2")
            plt.xlabel("x")
            plt.ylabel("psi(x)**2")
            plt.grid()
            plt.savefig("psi.png")
        
        except:
            print("Error al graficar la funcion de onda")
            print("Asegurese de que la funcion de onda sea una funcion de sympy")
            print("y que los limites de la integral sean numeros")
        else:
            print("La grafica de la funcion de onda se guardo en el archivo psi.png")

    def create_analytical_integral_gif(self, a_values, b_values, filename='analytical_integral_animation.gif'):
        """ Método para crear un GIF que muestra la evolución de la integral analítica con diferentes intervalos """

        frames = []

        for a, b in zip(a_values, b_values):
            x = np.arange(self.n)
            self.a = a
            self.b = b
            muda = 1
            # y_analitica = [self.area(i) for i in x]
            fa = self.area(muda)

            plt.figure(figsize=(8, 6))
            plt.title(f'Probabilidad entre  (a={a:.2} y  b={b:.2})')
            plt.plot(fa,'. ' , label='Probabilidad')
            plt.grid()
            plt.xlabel('Numero de iteraciones')
            plt.ylabel('Probabilidad')
            
            # plt.legend()

            # Guardar el gráfico como una imagen temporal
            temp_filename = f'temp_analytical_plot_{a}_{b}.png'
            plt.savefig(temp_filename)
            frames.append(Image.open(temp_filename))
            plt.close()
            muda = muda + 1

        # Guardar las imágenes en un GIF
        frames[0].save(
            filename, 
            save_all=True, append_images=frames[1:], 
            duration=200, loop=0
        )

        # Eliminar las imágenes temporales
        for temp_filename in [f'temp_analytical_plot_{a}_{b}.png' for a, b in zip(a_values, b_values)]:
            os.remove(temp_filename)

        print(f'GIF guardado como {filename}')