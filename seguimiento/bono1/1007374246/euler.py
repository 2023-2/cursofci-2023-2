import numpy as np
import matplotlib.pyplot as plt
import sympy as sym
from sympy import symbols, Function, Eq, dsolve
from scipy import integrate
import sys
sym.init_printing(use_latex='mathjax')
plt.style.use("seaborn")

class edo:
    """
    Esta clase resuelve ecuaciones diferenciales de la forma y' = f(x,y)
    Entradas:
    * a: Limite inferior del intervalo
    * b: limite superior del intervalo
    * n: número de particiones del intervalo. Mejor precision con n grande.
    * y0: condicion inicial y(0)=y0
    * punto (opcional): Punto en que se desea evaluar la función
    salida:
    * plot con soluciones por método analitico, Euler y Runge-Kutta 
    """
    def __init__(self,a,b,n,y0,f,punto=False):
        self.a=a
        self.b=b
        self.n=n
        self.y0 = y0
        self.punto=punto
        self.x = []
        self.y = []
        self.f = f

    def h(self):
        if self.punto:
            return (self.punto - self.a)/self.n
        else:
            return (self.b - self.a)/self.n
        
    def X(self):
        if self.punto:
            self.x = np.arange(self.a, self.punto+self.h(), self.h()) #Como no hay punto se integra sobre todo el intervalo
        else:
            self.x = np.arange(self.a, self.b+self.h(), self.h()) # Se integra hasta el valor del punto

        return self.x
    
    def euler(self):
        self.X() #Se inician los valores para x
        self.y = [self.y0] #Se inicia la lista para guardar y con la condicion inicial
        self.xn = [self.x[0]] #Se inicia la lista para guardar x con la condicion inicial
        for i in range(len(self.x)): #se recorre todo el dominio
            self.y.append(self.y[i]+self.h()*self.f(self.x[i],self.y[i])) #Se calcula el y_n usando la ec de Euler
            self.xn.append(self.x[i]) #Se almacena el valor de x
        return self.xn, self.y
    
    def solanalitica(self): #solve_ivp
        self.X()
        self.y = []
        x = sym.Symbol('x') #Variable independiente
        y = sym.Function('y')(x) #Varuable dependiente
        diff_eq = Eq(y.diff(x), self.f(x, y)) #Se asigna la función al metodo Eq de Sympy
        sol = dsolve(diff_eq, y, ics={y.subs(x, self.a): self.y0}) #Se soluciona la ec diferencial
        for i in range(len(self.x)):
            self.y.append(sol.rhs.subs(x, self.x[i])) #Se almacenan los resultados para y
        return self.x, self.y
    
    def equation(self):
        self.X()
        self.y = []
        x = sym.Symbol('x')
        y = sym.Function('y')(x)
        diff_eq = Eq(y.diff(x), self.f(x, y))
        return diff_eq #Retorna la ecuación diferencial

        
    
    def RNK4(self):
        self.X()
        self.y = [self.y0]

        for i in range(self.n):
            k1 = self.f(self.x[i],self.y[i]) #Se calcula k1
            k2 = self.f(self.x[i]+0.5*self.h(), self.y[i]+0.5*k1*self.h()) #se calcula k2
            k3 = self.f(self.x[i]+0.5*self.h(), self.y[i]+0.5*k2*self.h()) #se calcula k3
            k4 = self.f(self.x[i]+    self.h(), self.y[i]+    k3*self.h()) #se calcula k4
            self.y.append(self.y[i] + (1/6)*self.h()*(k1+2*k2+2*k3+k4)) #Se calcula el y_n en funcion de k1,k2,k3,k4
        return self.x, self.y
    
    def figPlot(self):
        plt.figure(figsize = (10,5))
        try:
            xeuler, yeuler = self.euler() #Se llama la función Euler
        except:
            print("ERROR: Fallo en al ejecutar edo.euler()")

        try:
            xanalitica, yanalitica = self.solanalitica() #Se llama la solanalitica
        except:
            print("ERROR: Fallo en al ejecutar edo.solanalitica()")
        
        try: 
            xrnk, yrnk = self.RNK4() #Se llama la funcion Runge Kutta
        
        except:
            print("ERROR: Fallo en al ejecutar edo.RNK()")
        try:    
            plt.plot(xanalitica,yanalitica,label = "Analitica",linewidth=4,color = "lime") #Se hace el plot
            plt.plot(xeuler,yeuler,label = "Euler",color = "gold")
            plt.plot(xrnk,yrnk,label = " RNK4",linestyle='dashed',color = "black")
            plt.xlabel("x")
            plt.ylabel("$f(x)$")
            plt.title("Equation solutions")
            plt.legend()
            plt.savefig("SolEDO.png")
        except:
            print("ERROR: No se puedo hacer el plot. Revise función y parametros.")

    