import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

class EDO:
    
    """
    Esta clase permite solucionar ecuaciones diferenciales ordinarias
    de primer orden (dy/dx = f(x,y))
    
    Args:
        a: Límite inferior del intérvalo
        b: Límite superior del intérvalo
        n: Número de divisiones
        y_0: Condición inicial para y
        f: función de la EDO (dy/dx = f(x,y))
        punto: si se quiere acotar el intérvalo a un punto
        
    Returns
        Retorna los arreglos x y y(x) como la solución de la
        EDO, usando distintos métodos (Solución analítica, Euler y RK4).
        Además retorna la gráfica y(x) de la solución obtenida por cada 
        uno de los métodos
    """
    def __init__(self,a,b,n,y_0,f,punto=False):
        self.a=a
        self.b=b 
        self.n=n 
        self.punto = punto
        self.x=[]
        self.y=[]
        self.y_0=y_0
        self.f=f
        if self.punto:
            self.b = self.punto
        else:
            pass

    def h(self):  #Salto entre puntos contiguos
        return (self.b-self.a)/self.n
    
    
    def X(self):   #Arreglo de números para los valores de x
        self.x = np.arange(self.a,self.b+self.h(),self.h())
        return self.x 

        
    def euler(self):   #Método de Euler para la solución
        self.X()
        self.y=[self.y_0]
        for i in range(self.n):
            self.y.append(self.y[i]+self.h()\
                        *self.f(self.x[i],self.y[i]))
        return self.x, self.y
    
    def RK4(self):    #Método de RK4 para la solución 
        self.X()
        self.y=[self.y_0]
        
        for i in range(self.n):
            
            k1 = self.f(self.x[i], self.y[i])
            k2 = self.f(self.x[i] + self.h()/2, self.y[i] + (k1/2)*self.h())
            k3 = self.f(self.x[i] + self.h()/2, self.y[i] + (k2/2)*self.h())
            k4 = self.f(self.x[i] + self.h(), self.y[i] + k3*self.h())
        
            self.y.append(self.y[i] + (1/6)*self.h()*(k1 + 2*k2 + 2*k3 + k4))
        
        return self.x, self.y
    
    def solanalitica(self):   #Solución analítica
        t_eval = np.linspace(self.a,self.b,self.n)
        sol = solve_ivp(self.f,[self.a, self.b],[self.y_0], t_eval=t_eval)
        return sol.t, sol.y[0]
    
    def figPlot(self):   #Grafica los resultados
        plt.figure(figsize=(10,5))
        xeuler, yeuler = self.euler()
        xanal, yanal = self.solanalitica()
        xrk4, yrk4 = self.RK4()
        plt.plot(xeuler, yeuler, label="Solución Euler")
        plt.plot(xanal, yanal, label='Solucion analítica')
        plt.plot(xrk4, yrk4, label='Solucion RK4')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Solución de la EDO')
        plt.legend()
        plt.show()