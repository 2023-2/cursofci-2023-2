import numpy as np
import matplotlib.pyplot as plt
import sympy as sp


class solEDO:
    """
    Soluciona una ecuacion diferencial ordinaria de una función
    continua en el intervalo (x_init,lim_sup) con condicion
    inicial y_init, con N intervalos, de 3 maneras posibles
    y se comparan en un plot
    """
    def __init__(self,func,x_init,y_init,lim_sup, N,punto = False):
        self.f = func
        self.a = x_init
        self.yo = y_init
        self.b = lim_sup
        self.punto = punto
        self.N = N


        #Se define un array vaco para la variable x y un
        #array de ceros para el resultado de la integral
        self.x=[]
        try:
            self.sol=np.zeros(self.N+1)
        except:
            print("INGRESE UN VALOR ADECUADO PARA N")
        



    def h(self):
        """
        Define un incremento diferente si se ingresa el punto o no
        """
        try:
            if self.punto:
                h = (self.punto-self.a)/(self.N)
            else:
                h = (self.b-self.a)/(self.N)
            return h
        
        except:
            print("No se calculo el incremento")
        
    def X(self):

        """
        Define unlinspace para X diferente si se ingresa punto o no
        """
        try:
            if self.punto:
                self.x= np.linspace(self.a,self.punto+self.h(),self.N+1)
            else:
                self.x= np.linspace(self.a,self.b+self.h(),self.N+1)
            return self.x

        except:
            print("No se calculo el dominio")

    def solEuler(self):
        """
        Soluciona la ec. diferencial por el metodo de euler (RK1)
        """
        try:
            self.X()    #iniciar el X
            self.sol[0]=self.yo     #El primer valor de y es la cond. inicial
            yn=self.yo  #el primer yn es la cond. inicial
            for n in range(self.N):
                self.sol[n+1] = yn + self.h() * self.f(self.x[n],yn)
                yn=self.sol[n+1]    #Se redefine el yn

            return self.sol
        
        except:
            print("No se calculo el RK1")

    def solRK4(self):
        """
        Soluciona la ec. diferencial por el metodo de RK4
        """
        try:
            self.X() #Se definen X y yn igual que en Euler
            self.sol[0]=self.yo
            yn=self.yo
            
            for n in range(self.N):
                #Se calculan los k
                k1=self.f(self.x[n], yn)
                k2=self.f(self.x[n] + 0.5*self.h(), yn + 0.5*k1*self.h())
                k3=self.f(self.x[n] + 0.5*self.h(), yn + 0.5*k2*self.h())
                k4=self.f(self.x[n] + self.h(), yn + k3*self.h())
                #se calcula la solucion y se asigna al valor n+1 del array de ceros
                self.sol[n+1] = yn + self.h()/6*( k1 + 2*k2 + 2*k3 + k4) 
                yn=self.sol[n+1]

            return self.sol
        
        except:
            print("No se calculo el RK4")

    def solanal(self):
        """
        Soluciona la ec. diferencial por metodo analtico
        con calculo simbolico
        """
        try:
            #Se definen las vbles simbolicas
            x = sp.symbols('x')
            y = sp.Function('y')(x)

            #Se define la funcion como simbolica
            edo = sp.Eq(y.diff(x), self.f(x,y))
            self.latex=sp.latex(edo)
            #Se resuelva la ec diferencial
            sol = sp.dsolve(edo, y, ics={y.subs(x,self.a):self.yo})
            
            #Se define un funcion lambda de python a partir de la simbolica
            func= sp.lambdify(x, sol.rhs,"numpy")
        
            return  func(self.X())

        except:
            print("No se calculo la solucion analtica")

    def plot(self):
        try:
            #Se grafican las 3 soluciones
            plt.plot(self.X(),self.solEuler(),label="Sol. Euler (RK1)")
            plt.plot(self.X(),self.solRK4(),label="Sol. Euler (RK4)")
            plt.plot(self.X(),self.solanal(),label="Sol. analtica (SymPy)")
            #Detalles de grafica
            plt.xlabel("x")
            plt.ylabel("y")
            plt.title(f"Solucion a ${self.latex}$\nCon n={self.N}")
            plt.legend()
            #Guardar grafica
            plt.savefig("plot.png")

        except:
            print("No se pudo graficar")

       
        
        
    

    

    
    