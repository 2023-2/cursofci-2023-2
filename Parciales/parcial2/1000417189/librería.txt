Librería milosmooth

pip install milosmooth==0.1.12

import milosmooth as Msm

path='Colombia_COVID19_Coronavirus_casos_diarios.csv' #Colocar el url 
prob1=Msm.smoother(path,10)  # el parametro es proporcional al suavizado #Inicializar la clase
print(prob1.plotComparativo()) #Plotea la comparación

#Los siguientes plotean cada suavizado con los datos:

print(prob1.kernelGauss())
print(prob1.kernelEpon())
print(prob1.kernelTricube())
